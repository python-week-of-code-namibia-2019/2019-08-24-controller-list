# Python Week Of Code, Namibia 2019

## Task for Instructor

We want a new view to list the posts published in a given month.

1. Add the URL

   ```
   path('blog/<int:year>/<int:month>/', views.month_archive),
   ```

   to in [blog/urls.py](blog/urls.py).
2. Create the function `month_archive` in [blog/views.py](blog/views.py).

   ```
   def month_archive(request, year, month):
       posts = get_list_or_404(
           Post,
           published_date__gte=datetime(year, month, 1),
           published_date__lte=datetime(year, month, 31),
       )

       return render(
           request,
           'blog/month.html',
           {
               "year": year,
               "month": month,
               "posts": posts,
           }
       )
   ```
3. Create the template [blog/templates/blog/month.html](blog/templates/blog/month.html).

## Tasks for Learners

None.